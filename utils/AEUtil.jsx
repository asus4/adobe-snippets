#include 'json2.js'

if (typeof AEUtil !== 'object') {
  AEUtil = {};
}

(function () {

  function log(msg, tab) {
    if (tab != undefined && tab > 0) {
      for (var i=0; i<tab; ++i) {
        msg = "\t" + msg;
      }
    }
    $.writeln(msg);
  }

  function readJsonFile(path) {
    var file = new File(path);
    if(!file.open('r')) {
      log('fail to load file');
      return false;
    }
    var text = file.read();
    file.close();
    return JSON.parse(text);
  }

  function exportToFile(path, str) {
    if (!path) {
      log('path is undefined');
      return false;
    }
    var file = new File(path);
    if (!file.open("w")) {
      log('fail to open file');
      return false;
    }
    log(file);
    try {
      file.write(str);
    }
    catch(e) { // Might be security error.
      log(e);
    }
    file.close();
    return true;
  }

  //------------------
  // exports

  AEUtil.readJsonFile = readJsonFile;
  AEUtil.exportToFile = exportToFile;

}).call($);

