#target aftereffects
#include '../utils/AEUtil.jsx'

(function () {
  function log(msg) {
    $.writeln(msg);
  }

  var layers = app.project.activeItem.layers;

  for (var i=1; i<=95; i++) {
    var layerL = layers.byName('l'+i+'.png');
    var layerR = layers.byName('r'+i+'.png');
    layerR.moveAfter(layerL);
    log('L:'+layerL.index+ ' R:'+layerR.index);
  }

}).call($);