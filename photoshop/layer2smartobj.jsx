#target photoshop
#include '../utils/PSUtil.jsx'

(function () {
  function log(msg) {
    $.writeln(msg);
  }

  var layers = PSUtil.getArtLayers();
  for(var i=0; i<layers.length; i++) {
    PSUtil.convertToSmartObject(layers[i]);
  }

}).call($);